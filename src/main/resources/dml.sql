INSERT INTO student
VALUES (101, 'Violet Haudge',87014523215, 1901),
       (102, 'Tom Brown',87475421217, 1901),
       (103, 'Oliver Barber',87022586443, 1902)
;
INSERT INTO group_table
VALUES (1901, 'medical school'),
       (1902, 'school of design'),
       (1903, 'business school')
;