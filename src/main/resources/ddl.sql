CREATE TABLE student (
    id int,
    studName varchar(255),
    phone varchar(255),
    groupId int,
    PRIMARY KEY(id)
);
CREATE TABLE group_table (
    groupId int,
    name varchar(255),
    PRIMARY KEY(groupId)
);
ALTER TABLE student
ADD FOREIGN KEY (groupId) REFERENCES group_table(groupId);