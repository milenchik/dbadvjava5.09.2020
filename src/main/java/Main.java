import java.sql.*;
public class Main {
    public static void main(String[] args){
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5433/postgres",
                            "postgres", "12345");
            c.setAutoCommit(false);
            System.out.println("Opened successfully");

            stmt = c.createStatement();


            c.commit();

            ResultSet rs = stmt.executeQuery( "SELECT * FROM student;" );
            while ( rs.next() ) {
                int id = rs.getInt("id");
                String  name = rs.getString("studName");
                String phone  = rs.getString("phone");
                int  groupId = rs.getInt("groupId");

                System.out.println( "ID = " + id );
                System.out.println( "NAME = " + name );
                System.out.println( "Phone = " + phone );
                System.out.println( "group ID = " + groupId );

                System.out.println();
                Student student= new Student();
                System.out.println(student.toString());
            }
            ResultSet gr = stmt.executeQuery( "SELECT * FROM group_table;" );
            while ( gr.next() ) {
                int id = gr.getInt("groupId");
                String  name = gr.getString("name");


                System.out.println( "ID = " + id );
                System.out.println( "NAME = " + name );

                System.out.println();

            }
            rs.close();
            gr.close();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        System.out.println("DONE");
    }

}
