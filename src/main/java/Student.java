public class Student {

        private int id;
        private String studName;
        private String phone;
        private int groupId;

        @Override
        public String toString(){
            return "Student [id=" + id +", name=" + studName +", phone= "+ phone +" , group id= "+ groupId +"]";
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getStudName() {
            return studName;
        }

        public void setStudName(String studName) {
            this.studName = studName;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public int getGroupId() {
            return groupId;
        }

        public void setGroupId(int groupId) {
            this.groupId = groupId;
        }

}
